<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

use DateTimeImmutable;
use DateTimeZone;
use PHPUnit\Framework\Assert;

class FeedTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    private function getFeed(): Feed
    {
        $feed = new Feed('feed &amp; title', 'http://example.org/', 'feed & description');
        $feed->setPubDate(new DateTimeImmutable('2020-02-05 17:08:45', new DateTimeZone('UTC')));
        $feed->addCategory(new Category('categoryA', 'http://example.org/taxonomyA'));
        $feed->addCategory(new Category('categoryB, categoryC', 'http://example.org/taxonomyB', 'label & B'));
        $feed->setImage(new Image('http://example.org/image.jpg', 150, 75, 'image & description'));
        $feed->setCopyright('copyright & copyright');
        // RSS2
        $feed->setLanguage('en');
        $feed->setManagingEditor(new Person('John & Doe', 'john@example.org'));
        $feed->setWebMaster(new Person('Jane & Doe', 'jane@example.org'));
        $feed->setRating('rating & rating');
        $feed->setTtl(180);
        $feed->setSkipHours(array_merge([ -1, 24], range(0, 8)));
        $feed->setSkipDays(['Monday', 'Friday', '', 'invalid']);
        // Atom
        $feed->addAuthor(new Person('James & Doe', 'james@example.org', 'http://example.org/james'));
        $feed->addAuthor(new Person('Jacob Doe', 'jacob@example.org', 'http://example.org/jacob'));
        $feed->addContributor(new Person('Jarvis & Doe', 'jarvis@example.org', 'http://example.org/jarvis'));
        $feed->addContributor(new Person('Jonathan Doe', 'jonathan@example.org', 'http://example.org/jonathan'));
        $feed->setIcon('http://example.org/icon.png');

        $item = new FeedItem('item &amp; & title', 'http://example.org/item?utm_source=feed&utm_media=feed1', "item & perex ' with entities");
        $item->addCategory(new Category('category & category'));
        $item->addCategory(new Category('categoryW', 'http://example.org/taxonomy', 'labelW'));
        $item->setEnclosure(new Enclosure('http://example.org/file.mp3', 123456, 'audio/mpeg'));
        $item->setGuid('http://example.org/item', true);
        $item->setPubDate(new DateTimeImmutable('2020-02-03 09:17:13', new DateTimeZone('UTC')));

        $item->setSource(
            new Source(
                'external feed',
                'http://example.org/ext-rss',
                new DateTimeImmutable('2020-02-05 20:33:38', new DateTimeZone('UTC'))
            )
        );

        // Atom
        $item->setContent(new Content('<p>content</p>', 'html'));
        $item->addContributor(new Person('Jessica Doe', 'jessica@example.org', 'http://example.org/jessica'));
        $item->addContributor(new Person('Jonas Doe', 'jonas@example.org', 'http://example.org/jonas'));
        $item->setRights('copyright');

        // RSS2
        $item->setAuthor(new Person('Jessica Doe', 'jessica@example.org'));
        $item->setComments('http://example.org/comments');
        $feed->addItem($item);

        $itemXhtml = new FeedItem('item title2', 'http://example.org/item2?utm_source=feed&amp;utm_media=feed1', 'item2 perex');
        $itemXhtml->setGuid('id2', false);
        $itemXhtml->setPubDate(new DateTimeImmutable('2020-02-04 09:12:10', new DateTimeZone('UTC')));
        $itemXhtml->setContent(new Content("<p>content\nsecond line</p>", 'xhtml'));
        $feed->addItem($itemXhtml);

        $itemText = new FeedItem('item title3', 'http://example.org/item3?utm_source=feed&utm_media=feed1', 'item3 perex');
        $itemText->setGuid('id3', false);
        $itemText->setPubDate(new DateTimeImmutable('2020-02-05 09:12:10', new DateTimeZone('UTC')));
        $itemText->setContent(new Content("content\nsecond line", 'text'));
        $feed->addItem($itemText);

        $feed->setLastBuildDate(new DateTimeImmutable('2020-02-05 16:10:12', new DateTimeZone('UTC')));

        return $feed;
    }

    public function testRss2(): void
    {
        $xml = $this->getFeed()->createRss2('http://example.org/rss2.xml');
        Assert::assertXmlStringEqualsXmlFile(codecept_data_dir() . 'rss2.xml', $xml);
    }

    public function testAtom(): void
    {
        $xml = $this->getFeed()->createAtom('http://example.org/atom.xml');
        Assert::assertXmlStringEqualsXmlFile(codecept_data_dir() . 'atom.xml', $xml);
    }
}
