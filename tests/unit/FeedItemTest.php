<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

class FeedItemTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function testConctructorWithEmptyParameters(): void
    {
        $this->tester->expectThrowable(
            new \InvalidArgumentException('all parameters must be filled'),
            static function () {
                new FeedItem('', '', '');
            }
        );
    }
}
