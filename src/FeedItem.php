<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

use DateTimeImmutable;
use InvalidArgumentException;

class FeedItem
{

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $comments = '';

    /**
     * @var ?Enclosure
     */
    private $enclosure;

    /**
     * @var string
     */
    private $guid = '';

    /**
     * @var bool
     */
    private $guidIsPermalink = false;

    /**
     * @var DateTimeImmutable
     */
    private $pubDate;

    /**
     * @var Source
     */
    private $source;

    /**
     * RSS2
     * @var ?Person
     */
    private $author;

    /**
     * @var array<Category>
     */
    private $categories = [];

    /**
     * (Atom)
     * @var array<Person>
     */
    private $contributors = [];

    /**
     * full content (Atom)
     * @var Content
     */
    private $content;

    /**
     * (Atom) html
     * @var string
     */
    private $rights = '';

    public function __construct(string $title, string $link, string $description)
    {
        if (empty($title) || empty($link) || empty($description)) {
            throw new InvalidArgumentException('all parameters must be filled');
        }
        $link = str_replace('&amp;', '&', $link);
        $link = str_replace('&', '&amp;', $link);

        $this->title = $title;
        $this->link = $link;
        $this->description = $description;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     * @param bool $isPermalink true only if $guid is url
     */
    public function setGuid(string $guid, bool $isPermalink): void
    {
        $this->guid = $guid;
        $this->guidIsPermalink = $isPermalink;
    }

    public function isGuidIsPermalink(): bool
    {
        return $this->guidIsPermalink;
    }

    public function getPubDate(): ?DateTimeImmutable
    {
        return $this->pubDate;
    }

    public function setPubDate(DateTimeImmutable $pubDate): void
    {
        $this->pubDate = $pubDate;
    }

    public function getEnclosure(): ?Enclosure
    {
        return $this->enclosure;
    }

    public function setEnclosure(Enclosure $enclosure): void
    {
        $this->enclosure = $enclosure;
    }

    public function getSource(): ?Source
    {
        return $this->source;
    }

    public function setSource(Source $source): void
    {
        $this->source = $source;
    }

    public function getAuthor(): ?Person
    {
        return $this->author;
    }

    public function setAuthor(Person $author): void
    {
        $this->author = $author;
    }

    public function addCategory(Category $category): void
    {
        $this->categories[] = $category;
    }

    /**
     * @return array<Category> of categories with term, scheme, label
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    public function getComments(): string
    {
        return $this->comments;
    }

    /**
     * url of page with comments
     * @param string $comments
     */
    public function setComments(string $comments): void
    {
        $this->comments = $comments;
    }

    public function getContent(): ?Content
    {
        return $this->content;
    }

    public function setContent(Content $content): void
    {
        $this->content = $content;
    }

    public function addContributor(Person $person): void
    {
        $this->contributors[] = $person;
    }

    /**
     * @return array<Person>
     */
    public function getContributors(): array
    {
        return $this->contributors;
    }

    public function getRights(): string
    {
        return $this->rights;
    }

    public function setRights(string $rights): void
    {
        $this->rights = $rights;
    }
}
