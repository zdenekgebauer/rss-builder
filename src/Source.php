<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

use DateTimeImmutable;

class Source
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $url;

    /**
     * @var DateTimeImmutable
     */
    private $updated;

    public function __construct(string $name, string $url, DateTimeImmutable $updated)
    {
        $this->name = $name;
        $this->url = $url;
        $this->updated = $updated;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getUpdated(): DateTimeImmutable
    {
        return $this->updated;
    }
}
