<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

class Enclosure
{

    /**
     * @var string
     */
    private $url;

    /**
     * @var int
     */
    private $filesize;

    /**
     * @var string
     */
    private $contentType;

    public function __construct(string $url, int $filesize, string $contentType)
    {
        $this->url = $url;
        $this->filesize = $filesize;
        $this->contentType = $contentType;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getFilesize(): int
    {
        return $this->filesize;
    }

    public function getContentType(): string
    {
        return $this->contentType;
    }
}
