<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

class Person
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $uri;

    /**
     * @param string $name
     * @param string $email
     * @param string $uri homepage of person, (required for Atom)
     */
    public function __construct(string $name, string $email, string $uri = '')
    {
        $this->name = $name;
        $this->email = $email;
        $this->uri = $uri;
    }

    public function getLine(): string
    {
        return $this->email . ' (' . $this->name . ')';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getUri(): string
    {
        return $this->uri;
    }
}
