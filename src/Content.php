<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

/**
 * (Atom)
 */
class Content
{

    /**
     * @var string
     */
    private $value;

    /**
     * @var string
     */
    private $type;

    /**
     * @param string $value
     * @param string $type [text|html|xhtml]
     */
    public function __construct(string $value, string $type)
    {
        $this->value = $value;
        $this->type = $type;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
