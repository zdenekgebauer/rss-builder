<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

class Category
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $domain;

    /**
     * @var string
     */
    private $label;

    /**
     * @param string $name (for RSS2 more names delimited by comma)
     * @param string $domain (for Atom "scheme")
     * @param string $label (Atom)
     */
    public function __construct(string $name, string $domain = '', string $label = '')
    {
        $this->name = $name;
        $this->domain = $domain;
        $this->label = $label;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function getLabel(): string
    {
        return $this->label;
    }
}
