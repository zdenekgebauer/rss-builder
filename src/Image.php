<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

class Image
{

    /**
     * @var string
     */
    private $url;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var string
     */
    private $description;

    /**
     * for RSS2: image should be max 144x400px
     * for Atom: image should be twice as wide as they are tall
     *
     * @param string $url
     * @param int $width
     * @param int $height
     * @param string $description
     */
    public function __construct(string $url, int $width = 88, int $height = 31, string $description = '')
    {
        $this->url = $url;
        $this->width = $width;
        $this->height = $height;
        $this->description = $description;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getDescription(): string
    {
        return $this->description;
    }
}
