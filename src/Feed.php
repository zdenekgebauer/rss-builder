<?php

declare(strict_types=1);

namespace ZdenekGebauer\RssBuilder;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use DOMDocument;
use DOMElement;
use DOMNode;

class Feed
{

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $link;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $language = '';

    /**
     * @var string
     */
    private $copyright = '';

    /**
     * @var Person
     */
    private $managingEditor;

    /**
     * @var Person
     */
    private $webMaster;

    /**
     * @var DateTimeImmutable
     */
    private $pubDate;

    /**
     * @var ?DateTimeImmutable
     */
    private $lastBuildDate;

    /**
     * @var array<Category>
     */
    private $categories = [];

    /**
     * @var int
     */
    private $ttl = 0;

    /**
     * @var Image
     */
    private $image;

    /**
     * @var string
     */
    private $rating = '';

    /**
     * @var array<int>
     */
    private $skipHours = [];

    /**
     * @var array<string>
     */
    private $skipDays = [];

    /**
     * @var array<Person>
     */
    private $authors = [];

    /**
     * (Atom)
     * @var array<Person>
     */
    private $contributors = [];

    /**
     * (Atom)
     * @var string
     */
    private $icon = '';

    /**
     * @var array<FeedItem>
     */
    private $items = [];

    /**
     * @param string $title
     * @param string $link
     * @param string $description
     */
    public function __construct(string $title, string $link, string $description)
    {
        $this->title = $title;
        $this->link = rtrim($link, '/') . '/';
        $this->description = $description;
    }

    public function addItem(FeedItem $item): void
    {
        $this->items[] = $item;
    }

    /**
     * @param string $language ISO639-2 language code or code by W3C
     * @see https://www.w3.org/TR/REC-html40/struct/dirlang.html#langcodes
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    public function setCopyright(string $copyright): void
    {
        $this->copyright = $copyright;
    }

    /**
     * (RSS)
     * @param Person $managingEditor
     */
    public function setManagingEditor(Person $managingEditor): void
    {
        $this->managingEditor = $managingEditor;
    }

    /**
     * (RSS)
     * @param Person $webMaster
     */
    public function setWebMaster(Person $webMaster): void
    {
        $this->webMaster = $webMaster;
    }

    public function setPubDate(DateTimeImmutable $pubDate): void
    {
        $this->pubDate = $pubDate;
    }

    public function setLastBuildDate(DateTimeImmutable $lastBuildDate): void
    {
        $this->lastBuildDate = $lastBuildDate;
    }

    public function addCategory(Category $category): void
    {
        $this->categories[] = $category;
    }

    public function setImage(Image $image): void
    {
        $this->image = $image;
    }

    /**
     * (Atom)
     * @param string $icon url of image
     */
    public function setIcon(string $icon): void
    {
        $this->icon = $icon;
    }

    /**
     * (RSS)
     * @param string $rating
     * @see https://www.w3.org/PICS/
     */
    public function setRating(string $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * (RSS)
     * @param array<int> $skipHours hours in UTC timezone
     */
    public function setSkipHours(array $skipHours): void
    {
        $skipHours = array_map('\intval', $skipHours);
        $this->skipHours = array_filter(
            $skipHours,
            static function (int $hour) {
                return $hour >= 0 && $hour < 24;
            }
        );
    }

    /**
     * (RSS)
     * @param array<string> $skipDays english weekdays
     */
    public function setSkipDays(array $skipDays): void
    {
        $skipDays = array_map('\strval', $skipDays);
        $allowed = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $this->skipDays = array_intersect($skipDays, $allowed);
    }

    public function addAuthor(Person $author): void
    {
        $this->authors[] = $author;
    }

    /**
     * (Atom)
     * @param Person $contributor
     */
    public function addContributor(Person $contributor): void
    {
        $this->contributors[] = $contributor;
    }

    /**
     * (RSS)
     * @param int $ttl time to live in minutes
     */
    public function setTtl(int $ttl): void
    {
        $this->ttl = $ttl;
    }

    public function createAtom(string $feedUrl): string
    {
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;
        /** @var DOMElement $tagFeed */
        $tagFeed = $dom->appendChild(new DOMElement('feed'));
        $tagFeed->setAttribute('xmlns', 'http://www.w3.org/2005/Atom');
        $tagFeed->appendChild(new DOMElement('title', self::fixSpecialChars($this->title)));
        $tagFeed->appendChild(new DOMElement('id', $this->link));

        /** @var DOMElement $tagLink */
        $tagLink = $tagFeed->appendChild(new DOMElement('link'));
        $tagLink->setAttribute('rel', 'self');
        $tagLink->setAttribute('type', 'application/atom+xml');
        $tagLink->setAttribute('href', $feedUrl);
        /** @var DOMElement $tagSubtitle */
        $tagSubtitle = $tagFeed->appendChild(new DOMElement('subtitle', self::fixSpecialChars($this->description)));
        $tagSubtitle->setAttribute('type', 'html');
        $buildDate = ($this->lastBuildDate instanceof DateTimeImmutable
            ? $this->lastBuildDate : new DateTimeImmutable('now', new DateTimeZone('UTC')));
        $tagFeed->appendChild(new DOMElement('updated', $buildDate->format('c')));

        foreach ($this->authors as $author) {
            $tagAuthor = $tagFeed->appendChild(new DOMElement('author'));
            self::appendChildPersonAtom($tagAuthor, $author);
        }

        foreach ($this->categories as $category) {
            /** @var DOMElement $tagCategory */
            $tagCategory = $tagFeed->appendChild(new DOMElement('category'));
            $tagCategory->setAttribute('term', $category->getName());
            self::setAttrNonEmpty($tagCategory, 'scheme', $category->getDomain());
            self::setAttrNonEmpty($tagCategory, 'label', $category->getLabel());
        }

        foreach ($this->contributors as $contributor) {
            $tagContributor = $tagFeed->appendChild(new DOMElement('contributor'));
            self::appendChildPersonAtom($tagContributor, $contributor);
        }

        /** @var DOMElement $tagGenerator */
        $tagGenerator = $tagFeed->appendChild(new DOMElement('generator', 'ZdenekGebauer\RssBuilder'));
        $tagGenerator->setAttribute('uri', 'https://github.com/zdenekgebauer/rss-builder');
        $tagGenerator->setAttribute('version', '0.0.1');

        self::appendChildNonEmpty($tagFeed, 'icon', $this->icon);
        if ($this->image !== null) {
            self::appendChildNonEmpty($tagFeed, 'logo', $this->image->getUrl());
        }
        self::appendChildNonEmpty($tagFeed, 'rights', $this->copyright);

        $xhtmlUsed = false;
        foreach ($this->items as $item) {
            $tagItem = $tagFeed->appendChild(new DOMElement('entry'));
            $tagItem->appendChild(new DOMElement('id', $item->getLink()));
            $tagItem->appendChild(new DOMElement('title', self::fixSpecialChars($item->getTitle())));
            $tagItem->appendChild(new DOMElement('updated', $item->getPubdate()->format('c')));

            /** @var DOMElement $tagLink */
            $tagLink = $tagItem->appendChild(new DOMElement('link'));
            $tagLink->setAttribute('rel', 'alternate');
            $tagLink->setAttribute('type', 'text/html');
            $tagLink->setAttribute('href', str_replace('&amp;', '&', $item->getLink()));

            $tagItem->appendChild(new DOMElement('summary', self::fixSpecialChars($item->getDescription())));
            if ($item->getContent()) {
                $contentType = $item->getContent()->getType();
                if ($contentType === 'xhtml') {
                    $tagContent = $tagItem->appendChild(new DOMElement('content'));
                    $tagContentDiv = $tagContent->appendChild(new DOMElement('div'));
                    /** @var DOMElement $tagContentDiv */
                    $tagContentDiv->setAttribute('xmlns', 'http://www.w3.org/1999/xhtml');
                    $tagContentDiv->appendChild($dom->createCDATASection($item->getContent()->getValue()));
                    $xhtmlUsed = true;
                } elseif ($contentType === 'html') {
                    $tagContent = $tagItem->appendChild(new DOMElement('content'));
                    $tagContent->appendChild($dom->createCDATASection($item->getContent()->getValue()));
                } else {
                    $tagContent = $tagItem->appendChild(new DOMElement('content', $item->getContent()->getValue()));
                }
                /** @var DOMElement $tagContent */
                $tagContent->setAttribute('type', $contentType);
            }

            foreach ($item->getCategories() as $category) {
                /** @var DOMElement $tagCategory */
                $tagCategory = $tagItem->appendChild(new DOMElement('category'));
                $tagCategory->setAttribute('term', $category->getName());
                self::setAttrNonEmpty($tagCategory, 'scheme', $category->getDomain());
                self::setAttrNonEmpty($tagCategory, 'label', $category->getLabel());
            }

            foreach ($item->getContributors() as $contributor) {
                $tagContributor = $tagItem->appendChild(new DOMElement('contributor'));
                self::appendChildPersonAtom($tagContributor, $contributor);
            }
            $tagItem->appendChild(new DOMElement('published', $item->getPubDate()->format('c')));
            if (!empty($item->getRights())) {
                /** @var DOMElement $tagRights */
                $tagRights = $tagItem->appendChild(new DOMElement('rights', $item->getRights()));
                $tagRights->setAttribute('type', 'html');
            }

            if ($item->getEnclosure()) {
                /** @var DOMElement $tagEnclosure */
                $tagEnclosure = $tagItem->appendChild(new DOMElement('link'));
                $tagEnclosure->setAttribute('rel', 'enclosure');
                $tagEnclosure->setAttribute('href', $item->getEnclosure()->getUrl());
                $tagEnclosure->setAttribute('length', (string)$item->getEnclosure()->getFilesize());
                $tagEnclosure->setAttribute('type', $item->getEnclosure()->getContentType());
            }

            if ($item->getSource()) {
                $tagSource = $tagItem->appendChild(new DOMElement('source'));
                $tagSource->appendChild(new DOMElement('id', $item->getSource()->getUrl()));
                $tagSource->appendChild(new DOMElement('title', $item->getSource()->getName()));
                $tagSource->appendChild(new DOMElement('updated', $item->getSource()->getUpdated()->format('c')));
            }
        }

        $result = (string)$dom->saveXML();
        if ($xhtmlUsed) {
            $result = preg_replace(
                '/(<div xmlns="http:\/\/www.w3.org\/1999\/xhtml">)(<!\[CDATA\[)([\s\S]*)(\]\]>)(<\/div>)/mU',
                '$1$3$5',
                $result
            );
        }
        return $result;
    }

    public function createRss2(string $feedUrl): string
    {
        $dom = new DOMDocument('1.0', 'UTF-8');
        $dom->formatOutput = true;
        /** @var DOMElement $tagRss */
        $tagRss = $dom->appendChild(new DOMElement('rss'));
        $tagRss->setAttribute('version', '2.0');
        $tagRss->setAttribute('xmlns:atom', 'http://www.w3.org/2005/Atom');

        $tagChannel = $tagRss->appendChild(new DOMElement('channel'));
        $tagChannel->appendChild(new DOMElement('title', self::fixSpecialChars($this->title)));
        $tagChannel->appendChild(new DOMElement('link', $this->link));
        $tagChannel->appendChild(new DOMElement('description', self::fixSpecialChars($this->description)));
        self::appendChildNonEmpty($tagChannel, 'language', $this->language);
        self::appendChildNonEmpty($tagChannel, 'copyright', self::fixSpecialChars($this->copyright));
        self::appendChildPersonRss($tagChannel, 'managingEditor', $this->managingEditor);
        self::appendChildPersonRss($tagChannel, 'webMaster', $this->webMaster);
        self::appendChildDateRss($tagChannel, 'pubDate', $this->pubDate);
        self::appendChildDateRss($tagChannel, 'lastBuildDate', $this->lastBuildDate);

        foreach ($this->categories as $category) {
            $tagCategory = $tagChannel->appendChild(
                new DOMElement('category', self::fixSpecialChars($category->getName()))
            );
            self::setAttrNonEmpty($tagCategory, 'domain', $category->getDomain());
        }

        $tagChannel->appendChild(new DOMElement('generator', 'ZdenekGebauer/RssBuilder'));
        $tagChannel->appendChild(new DOMElement('docs', 'http://validator.w3.org/feed/docs/rss2.html'));
        if ($this->ttl > 0) {
            $tagChannel->appendChild(new DOMElement('ttl', (string)$this->ttl));
        }
        if ($this->image !== null) {
            $tagImage = $tagChannel->appendChild(new DOMElement('image'));
            $tagImage->appendChild(new DOMElement('url', $this->image->getUrl()));
            $tagImage->appendChild(new DOMElement('title', self::fixSpecialChars($this->title)));
            $tagImage->appendChild(new DOMElement('link', $this->link));
            $tagImage->appendChild(new DOMElement('width', (string)$this->image->getWidth()));
            $tagImage->appendChild(new DOMElement('height', (string)$this->image->getHeight()));
            self::appendChildNonEmpty($tagImage, 'description', self::fixSpecialChars($this->image->getDescription()));
        }
        self::appendChildNonEmpty($tagChannel, 'rating', $this->rating);

        if (!empty($this->skipHours)) {
            $tagHours = $tagChannel->appendChild(new DOMElement('skipHours'));
            foreach ($this->skipHours as $hour) {
                $tagHours->appendChild(new DOMElement('hour', (string)$hour));
            }
        }
        if (!empty($this->skipDays)) {
            $tagDays = $tagChannel->appendChild(new DOMElement('skipDays'));
            foreach ($this->skipDays as $day) {
                $tagDays->appendChild(new DOMElement('day', $day));
            }
        }

        $tagAtomLink = $dom->createElement('atom:link');
        $tagAtomLink->setAttribute('href', $feedUrl);
        $tagAtomLink->setAttribute('rel', 'self');
        $tagAtomLink->setAttribute('type', 'application/rss+xml');
        $tagChannel->appendChild($tagAtomLink);

        foreach ($this->items as $item) {
            $tagItem = $tagChannel->appendChild(new DOMElement('item'));
            $tagItem->appendChild(new DOMElement('title', self::fixSpecialChars($item->getTitle())));
            $tagItem->appendChild(new DOMElement('link', $item->getLink()));
            $tagItem->appendChild(new DOMElement('description', self::fixSpecialChars($item->getDescription())));
            self::appendChildPersonRss($tagItem, 'author', $item->getAuthor());

            foreach ($item->getCategories() as $category) {
                $tagCategory = $tagItem->appendChild(
                    new DOMElement('category', self::fixSpecialChars($category->getName()))
                );
                self::setAttrNonEmpty($tagCategory, 'domain', $category->getDomain());
            }
            self::appendChildNonEmpty($tagItem, 'comments', $item->getComments());

            if ($item->getEnclosure()) {
                /** @var DOMElement $tagEnclosure */
                $tagEnclosure = $tagItem->appendChild(new DOMElement('enclosure'));
                $tagEnclosure->setAttribute('url', $item->getEnclosure()->getUrl());
                $tagEnclosure->setAttribute('length', (string)$item->getEnclosure()->getFilesize());
                $tagEnclosure->setAttribute('type', $item->getEnclosure()->getContentType());
            }

            if ($item->getSource()) {
                /** @var DOMElement $tagSource */
                $tagSource = $tagItem->appendChild(new DOMElement('source'));
                $tagSource->setAttribute('url', $item->getSource()->getUrl());
            }

            $guid = $item->getGuid() === '' ? $item->getLink() : $item->getGuid();
            /** @var DOMElement $tagGuid */
            $tagGuid = $tagItem->appendChild(new DOMElement('guid', $guid));
            $tagGuid->setAttribute('isPermaLink', ($item->isGuidIsPermalink() ? 'true' : 'false'));
            self::appendChildDateRss($tagItem, 'pubDate', $item->getPubDate());
        }
        return (string)$dom->saveXML($tagRss);
    }

    private static function setAttrNonEmpty(DOMNode $tag, string $attributeName, string $attributeValue): void
    {
        if (!empty($attributeValue)) {
            /** @var DOMElement $tag */
            $tag->setAttribute($attributeName, $attributeValue);
        }
    }

    private static function appendChildNonEmpty(DOMNode $tag, string $childName, string $childValue): void
    {
        if (!empty($childValue)) {
            $tag->appendChild(new DOMElement($childName, self::fixSpecialChars($childValue)));
        }
    }

    private static function appendChildPersonRss(DOMNode $tag, string $childName, ?Person $person): void
    {
        if ($person !== null) {
            $tag->appendChild(new DOMElement($childName, self::fixSpecialChars($person->getLine())));
        }
    }

    private static function appendChildPersonAtom(DOMNode $tag, Person $person): void
    {
        $tag->appendChild(new DOMElement('name', self::fixSpecialChars($person->getName())));
        $tag->appendChild(new DOMElement('email', $person->getEmail()));
        $tag->appendChild(new DOMElement('uri', $person->getUri()));
    }

    private static function appendChildDateRss(DOMNode $tag, string $childName, ?DateTimeImmutable $date): void
    {
        if ($date instanceof DateTimeImmutable) {
            $tag->appendChild(new DOMElement($childName, $date->format(DateTimeInterface::RSS)));
        }
    }

    private static function fixSpecialChars(string $text): string
    {
        $decoded = htmlspecialchars_decode($text, ENT_QUOTES | ENT_XHTML);
        return htmlspecialchars($decoded, ENT_QUOTES | ENT_XML1);
    }
}
