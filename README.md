# RSS Builder 
Create feeds in format RSS2 and Atom by specifications  
http://www.rssboard.org/rss-specification and https://validator.w3.org/feed/docs/rfc4287.html

## Usage
```php
// create feed
$feed = new Feed('feed title', 'http://example.org/', 'feed description');
// set optional common properties of feed 
$feed->setPubDate(new DateTimeImmutable('2020-02-05 17:08:45', new DateTimeZone('UTC')));
$feed->addCategory(new Category('categoryA', 'http://example.org/taxonomyA'));
$feed->addCategory(new Category('categoryB, categoryC', 'http://example.org/taxonomyB', 'label B'));
$feed->setImage(new Image('http://example.org/image.jpg', 150, 75, 'image description'));
// set optional properties for RSS2
$feed->setLanguage('en');
$feed->setCopyright('copyright');
$feed->setManagingEditor(new Person('John Doe', 'john@example.org'));
$feed->setWebMaster(new Person('Jane Doe', 'jane@example.org'));
$feed->setRating('rating');
$feed->setTtl(180);
$feed->setSkipHours(array_merge([ -1, 24], range(0, 8)));
$feed->setSkipDays(['Monday', 'Friday', '', 'invalid']);
// set optional properties for Atom
$feed->addAuthor(new Person('James Doe', 'james@example.org', 'http://example.org/james'));
$feed->addAuthor(new Person('Jacob Doe', 'jacob@example.org', 'http://example.org/jacob'));
$feed->addContributor(new Person('Jarvis Doe', 'jarvis@example.org', 'http://example.org/jarvis'));
$feed->addContributor(new Person('Jonathan Doe', 'jonathan@example.org', 'http://example.org/jonathan'));
$feed->setIcon('http://example.org/icon.png');

// create feed item 
$item = new FeedItem('item title', 'http://example.org/item', 'item perex');
// set optional common properties of item  
$item->addCategory(new Category('categoryQ'));
$item->addCategory(new Category('categoryW', 'http://example.org/taxonomy', 'labelW'));
$item->setEnclosure(new Enclosure('http://example.org/file.mp3', 123456, 'audio/mpeg'));
$item->setGuid('http://example.org/item', true);
$item->setPubDate(new DateTimeImmutable('2020-02-03 09:17:13', new DateTimeZone('UTC')));
$item->setSource(new Source('external feed', 'http://example.org/ext-rss', new DateTimeImmutable('2020-02-05 20:33:38')));
// set optional properties for RSS2
$item->setAuthor(new Person('Jessica Doe', 'jessica@example.org'));
$item->setComments('http://example.org/comments');
// set optional properties for Atom
$item->setContent(new Content('<p>content</p>', 'html'));
$item->addContributor(new Person('Jessica Doe', 'jessica@example.org', 'http://example.org/jessica'));
$item->addContributor(new Person('Jonas Doe', 'jonas@example.org', 'http://example.org/jonas'));
$item->setRights('copyright');
 
$feed->addItem($item);
// ...add more items

// set last build date. It should be time of last change of content of feed 
$feed->setLastBuildDate(new DateTimeImmutable('2020-02-05 16:10:12', new DateTimeZone('UTC')));

// create XML in RSS2 format
$rss2 = $feed->createRss2('http://example.org/rss2.xml');
// and/or in Atom format 
$atom = $feed->createAtom('http://example.org/atom.xml');
```

## Know Issues
 * support only UTF-8 encoding
 * tag cloud (RSS2) is not supported
 * tag textInput (RSS2) is not supported
